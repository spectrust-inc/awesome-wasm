const path = require("path");

module.exports = function override(config, env) {
  /* *************************************************************************
   * WebAssembly Support
   * *************************************************************************
   * CRA by default will use a rule that removes WASM files from the final
   * distribution, even if you attempt to include them explicitly. Here, we
   * ensure that CRA's file-loader rule will not handle WASM files. We then
   * add a rule to use `wasm-loader` for WASM files, which includes them as
   * compiled data in the resulting bundle.
   */

  const wasmExtensionRegExp = /\.wasm$/;

  if (!config.module.rules) {
    config.module.rules = [];
  }

  // Ensure that file-loader ignores WASM files.
  // Sourced from https://prestonrichey.com/blog/react-rust-wasm/
  config.module.rules.forEach((rule) => {
    (rule.oneOf || []).forEach((oneOf) => {
      if (oneOf.loader && oneOf.loader.indexOf("file-loader") >= 0) {
        // Make file-loader ignore WASM files
        oneOf.exclude.push(wasmExtensionRegExp);
      }
    });
  });

  // Use wasm-loader for WASM files
  config.module.rules.push({
    test: wasmExtensionRegExp,
    // I'm going to level with you: I copied this in from the example, but I
    // have no idea why it's necessary. If it's not here, it breaks, though.
    include: path.resolve(__dirname, "src"),
    use: [{ loader: require.resolve("wasm-loader"), options: {} }],
  });

  // End WebAssembly Support
  // *************************************************************************

  return config
}
