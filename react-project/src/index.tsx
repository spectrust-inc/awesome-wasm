// bootstrap file to allow top-level imports of WASM modules.
// Actual entrypoint is in main.tsx.

import("./main").catch(err => alert(`Something went wrong: ${err}`))

// required to avoid typescript compilation issues with --isolatedModules
export { }
