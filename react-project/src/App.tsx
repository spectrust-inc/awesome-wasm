import * as rustLib from "@spectrust/rust-lib";
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Rust says that 2 + 2 = {rustLib.add(2, 2)}
        </p>
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
