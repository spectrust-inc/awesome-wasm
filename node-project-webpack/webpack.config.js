const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: "development",
  entry: [
    "./src/index.ts"
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      },
    ]
  },
  experiments: {
    "asyncWebAssembly": true
  },
  plugins: [
    // copy the wasm-containing library over wholesale
    new CopyPlugin({
      patterns: [
        {
          // copy the js directory
          from: "js",
          // from this folder
          context: path.resolve(__dirname, "../rust-lib/"),
          // to this path in our dist/ directory
          to: "node_modules/@spectrust/rust-lib",
        },
      ],
    })
  ],
  externals: {
    "@spectrust/rust-lib": "commonjs @spectrust/rust-lib",
  }
};
