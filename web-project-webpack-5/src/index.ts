import * as rustLib from "@spectrust/rust-lib";

function main() {
  document.body.innerHTML += `
    Rust thinks that 2 + 2 = ${rustLib.add(2, 2)}
  `
}

main()
