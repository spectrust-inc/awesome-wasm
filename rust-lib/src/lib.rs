//! rust-lib to expose some simple wasm functionality

// we don't need the whole prelude, but it doens't hurt.
use wasm_bindgen::prelude::*;

/// Add two numbers together.
///
/// We'll be suer to validate that we can actually call this function from the
/// various contexts where we use it.
#[wasm_bindgen]
pub fn add(first: usize, second: usize) -> usize {
    first + second
}
