#!/usr/bin/env bash

set -euo pipefail

# A robust if verbose way of getting the script's directory
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

make build -C "$SCRIPT_DIR/.."
