#!/usr/bin/env bash
# Build the javascript package

set -euo pipefail

# A robust if verbose way of getting the script's directory
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

# Ensure we're in rust-lib root directory
cd "$SCRIPT_DIR"

mkdir -p ./js

TARGETS="nodejs bundler"

echo "Building WASM projects"
# Build WASM bundles
for target in $TARGETS; do
    wasm-pack build --scope spectrust --target "$target" --out-dir "./js/$target"
done

# Remove extra files, including the package.json files in each project and
# duplicate type files. Move the type files out into their own directory,
# which we can point to from the top-level package.json

echo "Deduping type files"
# doesn't matter which type files we copy, they're all the same
mkdir -p ./js/types
cp ./js/bundler/rust_lib.d.ts ./js/types/rust_lib.d.ts

echo "Removing unneded files"
for target in $TARGETS; do
    rm -f "./js/$target/.gitignore" \
        "./js/$target/package.json" \
        "./js/$target/rust_lib.d.ts"
done
