# awesome-wasm

A repository of real, working examples of using rust-generated webassembly in a
variety of JS/TS contexts.

I have generally found the WASM documentation lacking when it comes to practical
examples. The examples tend to assume that you're just publishing something to
NPM, that you don't need to produce code for both node and the browser, and that
you only need to load it in an extremely simple browser context.

With that in mind, the intent here is to show some real-world examples of how
you might use a rust project to share logic across the javascript boundary in
your workspace or other multi-project context. This example repository is a
monorepo, so the patterns here will work directly in that context. However, a
primary goal of the way things are set up here is that _consumers_ of a library
should not need to know anything about where that library lives, so all of these
examples should also work if you are publishing your WASM package to a private
(or public) registry and consuming them in an entirely different repository.

## Getting Started

The easiest way to run this project yourself is with [nix] and [direnv]. Nix
will create an isolated development environment with all required dependencies,
and direnv will ensure the nix environment is active whenever you're working on
this project (you can use a direnv plugin for your editor of choice to ensure
the project is activated in that context, also).

If you are using nix:

- install nix and a direnv plugin for your editor
- run `make install`, or if for some reason you don't have `make`, run
  `./scripts/install.sh`

If you are not using nix:

- install rust, a recent version of make, node, and yarn
- ensure you have `fd` installed, either through your favorite package manager
  or via `cargo install`
- `cargo install wasm-bindgen-cli`
- `cargo install wasm-pack`
- run `yarn install` at the project root

## Layout

### Top-Level Configs

- `Cargo.toml` defines the rust workspace, which has only one member, `rust-lib`
- `package.json` defines the yarn workspace
  - the workspace should include the generated rust library, which we decided to
    put at `rust-lib/js`, although you could output it wherever you want
  - it should also include any other JS projects

### rust-lib

The top-level defines a Rust workspace, which contains only one crate:
`rust-lib`. The `rust-lib` crate is found in the [`/rust-lib`](/rust-lib)
directory.

The library exposes one function: `add()`, which takes two numbers and returns
their sum.

The Makefile in `rust-lib` defines how the JS packages are built. You obviously
can use whatever means you want to generate your packages, but the main thing is
that we build both wasm contexts (node + bundler) and provide a checked
in `package.json` that provides appropriate entrypoints into each of these
generated packages. This allows the same top-level package to be used by any
consuming library.

The JS artifacts are not checked in.

You can build them with `make build`, or by running `./rust-lib/build.sh`.

`make build` will only rebuild if source files have changed, so we can call it
as part of package pre-install without slowing anything down.

The only JS-related file that is checked in is `./rust-lib/js/package.json`.
This defines entrypoints into the various bundles, as discussed above.

### react-project

This is a barebones project created with `create-react-app` to demonstrate using
a webassembly module. It illustrates a few things:

- you need to add `wasm-loader` to your dev dependencies
- you need to use something like `react-app-rewired` or similar (or eject) so
  that you can adjust the webpack config. You can see the requisite overrides
  [here](/react-project/config-overrides.js)
  - the upshot of the overrides is that you need to exclude wasm files from the
    file loader that CRA uses and process them instead with `wasm-loader`
- the WASM library is imported in `App.tsx`. You'll note that the import needs
  to be asynchronous. In the real world, you'd want to be sure to just import
  this once and then re-use it.

You can run it with `make react-project-start` or `yarn react-project start`.

### web-project-webpack-4

This is a no-framework web project using webpack 4. Webpack 4 supports
webassembly out of the box, so our job is a bit simpler here, requiring no
updates at all to the webpack config, which is just set up to build the
typescript project.

The only particularities are:

- like with the react project above, we are using an asynchronous import for the
  webassembly-containing module
- we have to be sure to use `esnext` as our `module` type in our `tsconfig.json`

You can run it with `make web-project-webpack-4-start` or `yarn
web-project-webpack-4 start`. The project wil be running at http://localhost:8080

### web-project-webpack-5

This is exactly like `web-project-webpack-4` except it uses webpack 5. The only
difference from webpack 4 is that we have to add the following to our
`module.exports` in our webpack config:

```js
module.exports = {
  experiments: {
    "asyncWebAssembly": true
  }
}
```

### node-project

This is a regular old node project and is one of our easiest contexts to work
with. We just add our WASM-containing project to `package.json`, import it using
regular import statements, and then use it. No trickery required here.

### node-project-webpack

If you're bundling your node project, things are a bit trickier. You can't use
the `bundler` output, since it is made for browsers, and the `node` output
doesn't play well with webpack.

The solution is to _not_ bundle any webpack dependencies, and ensure they are
installed on your server in another way. For monorepo projects (and in this
example), you can use the `CopyPlugin` to put everything into a `node_modules`
directory in your distribution directory, then zip up and deploy the entire
distribution directory. Since node always looks for a `node_modules` directory
next to whatever file it's running, this should Just Work. Otherwise, you'll
want to run an `npm install` or similar on your server to ensure the non-bundled
dependency is available. We can do this by adding it to the `externals` property
in the webpack config exports and telling webassembly to expect it to be a
commonjs package.

``` js
module.exports = {
  externals: {
    "@spectrust/rust-lib": "commonjs @spectrust/rust-lib"
  }
}
```

The same webpack config rules apply as for the web projects above, so if using
webpack 5 you'll need to add that `experiments` section.

[direnv]: https://direnv.net/
[nix]: https://nixos.org/guides/install-nix.html
