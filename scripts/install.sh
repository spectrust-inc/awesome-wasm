#!/usr/bin/env bash

set -euo pipefail

if ! command -v nix-env >/dev/null; then
    echo "Sorry, automatic install only works with nix" 1>&2
fi

echo "Installing the minimum required packages in the user's nix environment"

echo "Checking for direnv..."
if ! command -v direnv >/dev/null; then
    echo "Installing direnv in the user's nix profile"
    nix-env -iA nixpkgs.direnv
fi

echo "Activating direnv, which will install nix packages. This may take a moment."
direnv allow

echo "Installing JS packages with yarn"
yarn install
