# shell.nix file to define the isolated local environment.
#
# This file defines a function (general form arguments: body) that takes an
# optional package set and calls mkShell to generate the local env.

# Define variables to be availble after `in`
let
  sources = import ./nix/sources.nix;
  rust_overlay = import sources.rust-overlay;
  # apply the rust-overlay to the packageset, making the rust-bin package
  # that we use available from the oxalica overlay, which we use to determine
  # the rust enviornment based on the toolchain file.
  nixpkgs = import sources.nixpkgs {
    overlays=[ rust_overlay ];
  };
in

# Allow passing in a custom package set if desired. Otherwise, use the pinned
# version.
{ pkgs ? nixpkgs }:

# pkgs is an attribute set (hash map / dictionary). `with` unpacks the properties
# from the attrset into the current namespace, so we can say just `gnumake`
# rather than `pkgs.gnumake`.
with pkgs;

mkShell {
  # all buildInputs will be available in our local environment.
  buildInputs = [
    # installs the version of rust and components specified in the toolchain file.
    (rust-bin.fromRustupToolchainFile ./rust-toolchain.toml)
    fd                # fast file searching, used for make targets
    gnumake           # give us a recent make (looking at you MacOS)
    nodejs_latest     # most recent stable node
    wasm-bindgen-cli  # lower-level interface to get rust in our JS
    wasm-pack         # oh no, we got some rust in our javascript
    yarn              # we use yarn workspaces in this project
  ] ++ lib.optional stdenv.isDarwin [
    # we potentially need some extra stuff building rust projects on mac.
    darwin.apple_sdk.frameworks.SystemConfiguration
    libiconv
  ];

  # This runs on each shell invocation. We use it to add node_modules' .bin
  # to the path, so you can run stuff from JS packages without using npx run
  # or yarn run or whatever
  shellHook = ''
    export PATH="$PWD/node_modules/.bin:$PATH"
  '';
}
