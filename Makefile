
BASH := $(shell command -v bash)
SHELL := $(BASH) -e
PACKAGE_JSONS = $(shell fd 'package\.json' .)

# Set everything up
install: .make/install
.make/install: .make shell.nix $(PACKAGE_JSONS) scripts/install.sh
	./scripts/install.sh
	touch .make/install
.PHONY: install

# Update any and all dependencies
update:
	@echo "Updating nix package sources..."
	niv update
.PHONY: update

clean:
	$(MAKE) -C ./rust-lib clean
.PHONY: clean

# Build WASM project (only builds if rust files have changed)
build-wasm:
	$(MAKE) -C ./rust-lib build
.PHONY: build

node-project-start: build
	$(MAKE) -C ./node-project start
.PHONY: node-project-start

node-project-webpack-start: build
	$(MAKE) -C ./node-project-webpack start
.PHONY: node-project-webpack-start

react-project-start: build
	$(MAKE) -C ./react-project start
.PHONY: react-project-start

web-project-webpack-4-start: build
	$(MAKE) -C ./web-project-webpack-4 start
.PHONY: web-project-webpack-4-start

web-project-webpack-5-start: build
	$(MAKE) -C ./web-project-webpack-5 start
.PHONY: web-project-webpack-5-start

.make:
	mkdir -p .make
